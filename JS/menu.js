const toTop = document.getElementById('btnToTop')
const items = document.getElementById('items')
const templateCard = document.getElementById('template-card').content

const fragment = document.createDocumentFragment()

let categoriaMenu = localStorage.getItem('categoria');


document.addEventListener('DOMContentLoaded', () => {
    fetchData()
})

// Acción boton volver al inicio
toTop.addEventListener('click', e =>{
    scrollToTop()
})

const scrollToTop = ()=>{
    window.scrollTo({
        top: 0,
        behavior: 'smooth'
    });
}



const fetchData = async () => {
    try {
        const res = await fetch(`https://www.themealdb.com/api/json/v1/1/filter.php?c=${categoriaMenu}`);
        
        const data = await res.json();
        console.log(categoriaMenu);
        pintarcards(data);

    } catch (error) {
        console.log(error)
    }
}

const pintarcards = data => {
    console.log(data.meals);
    data.meals.forEach(meals => {
        templateCard.querySelector('h5').textContent = meals.strMeal
        templateCard.querySelector('p').textContent = meals.idMeal
        templateCard.querySelector('img').setAttribute('src', meals.strMealThumb)
        
        const clone = templateCard.cloneNode(true)
        fragment.appendChild(clone)

    });
    items.appendChild(fragment)

}