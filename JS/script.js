const selectCliente = document.getElementById('select-cliente');
const selectCategoriaMenu = document.getElementById('select-categoria-menu');
const selectMenu = document.getElementById('select-menu-categoria');

let clientes = {};

document.addEventListener('DOMContentLoaded', () => {
    loadCliente()
    loadCategoriasMenu()
})

selectCliente.addEventListener("change", e => subirLocalStorageCliente(e.target.value))

selectCategoriaMenu.addEventListener("change", e => subirLocalStorageCategoria(e.target.value))



function loadCliente(){
    fetch("https://johnfredyb.github.io/ApiPersonas/Persona.json")
    .then(res => res.ok ? res.json() : Promise.reject(res))
    .then(json => {
        console.log(json)
        let options = `<option value="">Elige un cliente</option>`;
        clientes = json;
        clientes.Personas.forEach(cliente => options += `<option value="${cliente.ID}">${cliente.Nombre} ${cliente.Apellidos}</option>`);
        selectCliente.innerHTML = options;
    })
    .catch(err => {
        console.log(err);
        let message = err.statusText || "Ocurrio un error";
        selectCliente.nextElementSibling.innerHTML = `Error ${err.status}: ${message}`;
    })
}

function subirLocalStorageCliente(id){
    console.log(id);
    let cliente = clientes.Personas.find((persona) => persona.ID === parseInt(id));
    console.log(cliente);
    localStorage.setItem('cliente', JSON.stringify(cliente));
}

function loadCategoriasMenu(){
    fetch("https://www.themealdb.com/api/json/v1/1/list.php?c=list")
    .then(res => res.ok ? res.json() : Promise.reject(res))
    .then(json => {
        console.log(json)
        let $options = `<option value="">Elige una categoria del Menu</option>`;
        json.meals.forEach(categoria => $options += `<option value="${categoria.strCategory}">${categoria.strCategory}</option>`);
        selectCategoriaMenu.innerHTML = $options;
    })
    .catch(err => {
        console.log(err);
        let message = err.statusText || "Ocurrio un error";
        selectCategoriaMenu.nextElementSibling.innerHTML = `Error ${err.status}: ${message}`;
    })
}

function subirLocalStorageCategoria(categoria){
    localStorage.setItem('categoria', categoria);
}

  
  
  